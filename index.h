#include "code.h"

void add(node **head, note buf)
{
    node *p = (node*)malloc(sizeof(node));
    p->data = buf;
    p->next = (*head);
    (*head) = p;
}

void sort_by_field(node **head)
{
    if(head == NULL)
    {
        printf("Список пуст\n");
        return;
    }
    int c=1;
    printf("1.По номеру\n2.По имени\n3.По дате рождения\n\nВыберите номер поля для сортировки: ");

    while(c != 0)
    {
        scanf("%d", &c);
        switch(c)
        {
            case 1:
                sort_tele(head);
                c = 0;
            break;
            case 2:
                sort_name(head);
                c = 0;
            break;
            case 3:
                sort_bday(head);
                c = 0;
            break;
            default:
                printf("--->");
        }
    }
}

void sort_tele(node **head)
{
    if(*head == NULL)
    {
        printf("Список пуст\n");
        return;
    }

    node *p1 = *head;

    while(p1->next != NULL)
    {
        node *p2 = p1->next;
        do
        {
            if((p1->data.tele > p2->data.tele))
            {
                note buf = p2->data;
                p2->data = p1->data;
                p1->data = buf;
            }
            p2 = p2->next;
        }
        while(p2 != NULL);
        p1 = p1->next;
    }

}
void sort_name(node **head)
{
    if(*head == NULL)
    {
        printf("Список пуст\n");
        return;
    }

    node* p1 = *head;

    while(p1->next != NULL)
    {
        node *p2 = p1->next;
        do
        {
            if(strcmp(p1->data.name, p2->data.name)>0)
            {
                note buf = p2->data;
                p2->data = p1->data;
                p1->data = buf;
            }
            p2 = p2->next;
        }
        while(p2 != NULL);

        p1 = p1->next;
    }
}

void sort_bday(node** head)
{
    if(*head == NULL)
    {
        printf("Список пуст\n");
        return;
    }

    node* p1 = *head;

    while(p1->next!= NULL)
    {
        node* p2 = p1->next;
        do
        {
            if(p1->data.bday[2] > p2->data.bday[2])
            {
                note buf = p2->data;
                p2->data = p1->data;
                p1->data = buf;
            }
            p2 = p2->next;
            }
        while(p2 != NULL);
        p1 = p1->next;
    }
}

void search(node *stack)
{
    node *temp = stack;
    int choice, sTele, sBday[3], i = 0;
    char sName[30];

    printf("Поиск по:\n");
    printf("1.Имени\n");
    printf("2.Телефону\n");
    printf("3.Дате дня рождения\n");
    printf("--->");
    scanf("%d", &choice);

    switch(choice)
    {
    case 1:
        printf("Имя--->");
        fflush(stdin);
        fgets(sName, 29, stdin);

        while(temp != NULL)
            {
            i++;
            if(strcmp(temp->data.name, sName) == 0)
            {
                printf("=========================FIND===========================\n");
                printf("\tName: %s\n", temp->data.name);
                printf("-   Birthday: %d.%d.%d  \n", temp->data.bday[0], temp->data.bday[1], temp->data.bday[2]);
                printf("-   Tele: %d   \n", temp->data.tele);
                printf("========================================================\n");
            }
            else printf("Поиск...Не найдено[%d]\n", i);
            temp = temp->next;
        }
        break;
    case 2:
        printf("Телефон--->");
        scanf("%d", &sTele);

        while(temp != NULL)
            {
            i++;
            if(temp->data.tele == sTele)
            {
                printf("=========================FIND===========================\n");
                printf("\tName: %s\n", temp->data.name);
                printf("-   Birthday: %d.%d.%d  \n", temp->data.bday[0], temp->data.bday[1], temp->data.bday[2]);
                printf("-   Tele: %d   \n", temp->data.tele);
                printf("========================================================\n");
            }
            else printf("Поиск...Не найдено[%d]\n", i);
            temp = temp->next;
        }
        break;
    case 3:
        printf("ДР[День]--->");
        scanf("%d", &sBday[0]);
        printf("ДР[Месяц]--->");
        scanf("%d", &sBday[1]);
        printf("ДР[Год]--->");
        scanf("%d", &sBday[2]);
        while(temp != NULL)
            {
            i++;
            if(temp->data.bday[0] == sBday[0] && temp->data.bday[1] == sBday[1] && temp->data.bday[2] == sBday[2])
            {
                printf("=========================FIND===========================\n");
                printf("\tName: %s\n", temp->data.name);
                printf("-   Birthday: %d.%d.%d  \n", temp->data.bday[0], temp->data.bday[1], temp->data.bday[2]);
                printf("-   Tele: %d   \n", temp->data.name);
                printf("========================================================\n");
            }
            else printf("Поиск...Не найдено[%d]\n", i);
            temp = temp->next;
        }
        break;
    default:
        printf("Ошибка!\n");
        break;
    }
}

void write_stack_inbin(node *blocknote)
{
    FILE *bin_out;
    node *temp = blocknote;

    bin_out = fopen("output.bin", "wb");
    if(bin_out == NULL)
    {
        printf("Ошибка при открытии файла\n");
        getch();
    } else printf("\tДвоичный файл открыт\n");

    while(temp != NULL)
    {
        fwrite(&temp->data.name, sizeof(char), 29, bin_out);
        fwrite(&temp->data.tele, sizeof(int), 1, bin_out);
        fwrite(&temp->data.bday[0], sizeof(int), 1, bin_out);
        fwrite(&temp->data.bday[1], sizeof(int), 1, bin_out);
        fwrite(&temp->data.bday[2], sizeof(int), 1, bin_out);
        temp = temp->next;
    }
    printf("========================================================\n");
    printf("\t\tУСПЕШНО ЗАПИСАНО\n");
    fclose(bin_out);
}

void read_bin(node **stack)
{
    FILE *bin_read;
    note p;
    char name[30];
    int tele, bday[3];

    bin_read = fopen("output.bin", "rb");
    if(bin_read == NULL)
    {
        printf("Ошибка при открытии файла\n");
        getch();
    } else printf("\Открываем для чтения...\n");
    printf("========================================================\n");

    while(!feof(bin_read))
    {
        fread(&p.name, sizeof(char), 29, bin_read);
        printf("\tBIN_name: %s\n", p.name);
        fread(&p.tele, sizeof(int), 1, bin_read);
        printf("BIN_tele: %d\n", p.tele);
        fread(&p.bday[0], sizeof(int), 1, bin_read);
        fread(&p.bday[1], sizeof(int), 1, bin_read);
        fread(&p.bday[2], sizeof(int), 1, bin_read);
        printf("BIN_bday: %d %d %d\n", p.bday[0], p.bday[1], p.bday[2]);
        printf("========================================================\n");

        add(stack, p);
    }
}

void read_txt_inStack(node **head)
{
    note p;
    int buff;

    FILE *f1 = fopen("f1.txt", "r");

    if(!f1)
    {
        printf("Файл не найден!\n");
        return;
    }

    while(!feof(f1))
    {
        fgets(p.name, 29, f1);
        fscanf(f1, "%d", &p.tele);
        fscanf(f1, "%d %d %d", &p.bday[0], &p.bday[1], &p.bday[2]);
        fscanf(f1, "%d", &buff);

        printf("\n========================================================\n");
        printf("\n\tИмя: ");
        puts(p.name);
        printf("\n Телефон:%d",p.tele);
        printf("\n Год поступления на работу:%d %d %d",p.bday[0], p.bday[1], p.bday[2]);
        printf("\n========================================================\n");

        add(head, p);
    }
    fclose(f1);
}

void show_table(node *head)
{
    rev(&head);
    FILE *f2=fopen("f2.txt","w");

    fprintf(f2,"===================================================\n");
    fprintf(f2,"|   Телефон   |   Дата Рождения   |   Имя   |   \n");
    fprintf(f2,"===================================================\n");

    while(head!= NULL)
    {
        fprintf(f2, "|   %d   |   %d.%d.%d   |%s \n", head->data.tele, head->data.bday[0], head->data.bday[1], head->data.bday[2], head->data.name);
        head = head->next;
    }
    rewind(f2);
    fclose(f2);
}

void outsheet(node *head)
{
    node *temp = head;
    int lenght = 0;

    while(temp != NULL)
    {
        lenght++;
        temp = temp->next;
    }

    int i,count,page=1;
    char c;
    printf("Введите количество людей на странице: ");
    scanf("%d", &count);
    if(count >= lenght)
    {
        output(head);
        return;
    }

    printf("Чтобы перейти на следующую страницу нажмите Enter\n");
    while(head != NULL)
    {
        scanf("%c", &c);
        printf("Страница №%d\n", page);
        for(i=0;i<count;i++)
        {
            printf("========================================================\n\t");
            puts(head->data.name);
            printf("Телефон: %d\n", head->data.tele);
            printf("День Рождения: %d %d %d\n", head->data.bday[0], head->data.bday[1], head->data.bday[2]);
            head = head->next;
            if(head == NULL) break;
        }
    page++;
    }
}

void swap(node **head)
{
    node *buf1, *buf2;
    note temp;
    buf1 = buf2 = *head;
    int a, b, p_num;
    printf("Введите первый номер телефона структуры, которую нужно поменять: ");
    scanf("%d",&a);
    printf("Введите второй номер телефона структуры, которую нужно поменять: ");
    scanf("%d",&b);

    while(a != buf1->data.tele)
    {
        buf1 = buf1->next;
    }

    while(b != buf2->data.tele)
    {
        buf2 = buf2->next;
    }

    temp = buf1->data;
    buf1->data = buf2->data;
    buf2->data = temp;
}

void rev(node **head)
{

    if(*head == NULL)
    {
        printf("Список пуст\n");
        return;
    }
    node *p = NULL;

    while(*head!= NULL)
    {
        add(&p,(*head)->data);
        *head = (*head)->next;
    }
    *head = p;
}

void del(node **head)
{
    if(*head == NULL)
    {
        printf("Список пуст\n");
        return;
    }
    node *p = *head, *p2;
    node *len = *head;
    int a, lenght = 0;

    while(len != NULL)
    {
        lenght++;
        len = len->next;
    }

    printf("Длина списка: %d\n", lenght);
    printf("Введите номер человека, который нужно удалить: \n");
    scanf("%d", &a);


    while(a != p->data.tele)
    {
        p = p->next;
    }
    if(p == *head)
    {
        *head = (*head)->next;
    }
    else
    {
        p2 = *head;
        while(a+1 != p2->data.tele)
        {
            p2 = p2->next;
        }

        p2->next = p->next;
        free(p);
    }
}

void output(node *head)
{
    printf("=======================START============================\n");
    while(head != NULL)
    {
        printf("========================================================\n\t");
        puts(head->data.name);
        printf("Телефон: %d\n", head->data.tele);
        printf("День Рождения: %d %d %d\n", head->data.bday[0], head->data.bday[1], head->data.bday[2]);
        head = head->next;
    }
    printf("======================END===============================\n");
}

void show(node *head)
{
    node *temp = head;

    while(temp != NULL)
    {
        printf("========================================================\n");
        printf("\tName: %s\n", temp->data.name);
        printf("-   Birthday: %d.%d.%d  \n", temp->data.bday[0], temp->data.bday[1], temp->data.bday[2]);
        printf("-   Tele: %d   \n", temp->data.tele);
        temp = temp->next;
    }
    printf("========================================================\n");
    printf("\t\tSUCCESSFULLY READ\n");
}
