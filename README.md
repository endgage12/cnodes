
# Below are the main functions that are contained in the code.c file and are called from the main file main.c. The actions of each function are briefly indicated.
1 Reading from a text file
1.1. Opening a file
1.2. Reading data from a file
1.3. Allocating memory and entering data into the DS
1.4. Closing the file

2 Reading from a binary file (Fig. 4.2)
2.1. Opening a file
2.2. Checking for an empty file
2.3. Reading data from a file
2.4. Allocating memory and entering data into the DS
2.5. Closing the file

3 Sorting by field (fig. 4.3)
3.1. Selecting a field for sorting and then calling the function (Sorting by phone number, name, year of birth)
3.2. Bubble sort

4 Removing the specified item (Fig.4.4)
4.1. Checking for the presence of data
4.2. Loop through structure elements and find the desired element
4.3. Deleting an element, freeing memory and shifting the pointer 5. Exchange of two specified elements (Fig. 4.5)

5.1 Checking for the presence of data
5.2. Entering a phone number and subsequent exchange through an additional pointer to structures

6 Element search (fig. 4.6)
6.1. Checking for the presence of DS data
6.2. Calling an additional function to search accordingly

7 Output to a text file in the form of a table
7.1. Checking for the presence of DS data
7.2. Drawing a table
7.3. Output to text file

8 Display on the screen page by page (fig. 4.7)
8.1. Checking for the presence of data
8.2. Displays the selected number of flights until the list ends or the user enters “exit”

9 Output to binary file
9.1. Checking for the presence of data
9.2. Reverse elements
9.3. Opening a file and writing data
9.4. Move pointer and close file

10 Exit
10.1 Closing the program